<?php

namespace SellerControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use SellerControl\Filter\CustomerFilter;

class Customer extends Form {
    
    public function __construct($name = null, $options = []) {
        parent::__construct('user', $options);

        $this->setInputFilter(new CustomerFilter());
        $this->setAttributes(array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-label-left'));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setLabel("Nome: ")
                ->setAttributes(array('class' => 'form-control'));
        $this->add($name);

        $email = new \Zend\Form\Element\Email('email');
        $email->setLabel("Email: ")
                ->setAttributes(array('class' => 'form-control'));
        $this->add($email);

        $phone = new \Zend\Form\Element\Text('phone');
        $phone->setLabel("Telefone: ")
                ->setAttributes(array('class' => 'form-control', 'id' => 'inpPhone'));
        $this->add($phone);

        $cellphone = new \Zend\Form\Element\Text('cellphone');
        $cellphone->setLabel("Celular: ")
                ->setAttributes(array('class' => 'form-control', 'id' => 'inpCellphone'));
        $this->add($cellphone);

        $postalCode = new \Zend\Form\Element\Text('postal_code');
        $postalCode->setLabel("CEP: ")
                ->setAttributes([
                    'class' => 'form-control',
                    'id' => 'frm-postal-code',
                ]);
        $this->add($postalCode);

        $address = new \Zend\Form\Element\Text('address');
        $address->setLabel("Endereço: ")
                ->setAttributes([
                    'class' => 'form-control',
                    'id' => 'frm-address',
                ]);
        $this->add($address);

        $number = new \Zend\Form\Element\Text('number');
        $number->setLabel("Número: ")
                ->setAttributes([
                    'class' => 'form-control',
                ]);
        $this->add($number);

        $complement = new \Zend\Form\Element\Text('complement');
        $complement->setLabel("Complemento: ")
                ->setAttributes([
                    'class' => 'form-control',
                ]);
        $this->add($complement);

        $neighborhood = new \Zend\Form\Element\Text('neighborhood');
        $neighborhood->setLabel("Bairro: ")
                ->setAttributes([
                    'class' => 'form-control',
                    'id' => 'frm-neighborhood',
                ]);
        $this->add($neighborhood);

        $city = new \Zend\Form\Element\Text('city');
        $city->setLabel("Cidade: ")
                ->setAttributes([
                    'class' => 'form-control',
                    'id' => 'frm-city',
                ]);
        $this->add($city);

        $state = new \Zend\Form\Element\Text('state');
        $state->setLabel("Estado: ")
                ->setAttributes([
                    'class' => 'form-control',
                    'id' => 'frm-state',
                ]);
        $this->add($state);
        
        $csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel('Salvar')
                ->setAttributes(array(
                    'value' => 'SALVAR',
                    'class' => 'btn btn-success',
                ));
        $this->add($submit);

        $button = new \Zend\Form\Element\Button('button');
        $button->setLabel('Cancelar')
                ->setAttributes(array(
                    'value' => 'Cancelar',
                    'class' => 'btn btn-warning',
                    'id' => 'btn_cancel_request',
                ));
        $this->add($button);
    }
}