<?php

namespace SellerControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;

class Report extends Form {
    
    public function __construct($name = null, $modules = [], $options = []) {
        parent::__construct('report', $options);

        $this->setAttributes(['method' => 'post', 'role' => 'form']);
        
        $module = new Select();
        $module->setLabel("Selecione o Módulo: ")
                ->setName('module')
                ->setAttributes(['class' => 'form-control', 'id' => 'frm-module'])
                ->setValueOptions($modules);
        $this->add($module);
        
        $initial_date = new \Zend\Form\Element\Date('initial_date');
        $initial_date->setLabel("Data Inicial: ")
                ->setAttributes(['class' => 'form-control', 'id' => 'frm-initial-date']);
        $this->add($initial_date);

        $final_date = new \Zend\Form\Element\Date('final_date');
        $final_date->setLabel("Data Final: ")
                ->setAttributes(['class' => 'form-control', 'id' => 'frm-final-date']);
        $this->add($final_date);

        $csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Button('button');
        $submit->setLabel('Gerar ')
                ->setAttributes([
                    'class' => 'btn btn-warning',
                    'style' => 'width: 100%; margin-top: -2px',
                    'id' => 'btn-make',
                    'onclick' => 'makeReport()',
                ]);
        $this->add($submit);
    }
}