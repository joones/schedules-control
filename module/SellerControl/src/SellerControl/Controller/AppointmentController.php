<?php

namespace SellerControl\Controller;

use Core\Controller\CrudController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityManager;
use SellerControl\Event\CashierEvent;
use SellerControl\Event\SecurityEvent;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;


class AppointmentController extends CrudController
{
    protected $em; 
    protected $securityEvent;
    protected $authService;
    protected $cEvent;

	public function __construct(EntityManager $em, $entity, $form, $service, $controller, $route) {
        $this->em         = $em;
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;

        $this->securityEvent = new SecurityEvent($this->em);
        $this->cEvent = new CashierEvent($this->em);
    }

    public function getAuthService() {
        return $this->authService;
    }

    public function indexAction() {
        $list = $this->em
            ->getRepository($this->entity)
            ->findAppointments();

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }

    public function newAction() {
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $file = $_FILES['gallery'];
                $numFile = count(array_filter($file['name']));
               
                if ($numFile > 0) {
                    $files_dir = md5(time());
                    mkdir("./public_html/uploads/appointment/" . $files_dir, 0777);
                    $path = "./public_html/uploads/appointment/" . $files_dir;

                    for ($i = 0; $i < $numFile; $i++) {
                        $name = $file['name'][$i];
                        $tmp  = $file['tmp_name'][$i];

                        $extension = @end(explode('.', $name));
                        $new_name = rand() . ".$extension";

                        move_uploaded_file($tmp, $path . '/' . $new_name);
                    }

                    $data['gallery'] = $files_dir;
                }

                if (
                    !$this->securityEvent->securityVerify($data['customer']) ||
                    !$this->securityEvent->securityVerify($data['description']) ||
                    !$this->securityEvent->securityVerify($data['price']) ||
                    !$this->securityEvent->securityVerify($data['notes']) ||
                    !$this->securityEvent->securityVerify($data['time']) 
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $form));
    }

    public function editAction() {
        $form = $this->form;
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id', 0);
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($id);
        
        if ($this->params()->fromRoute('id', 0)) {
            $form->setData($entity->toArray());
        }
        
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $file = $_FILES['gallery'];
                $numFile = count(array_filter($file['name']));

                if ($numFile > 0) {
                    $appointment = $this->em->getRepository($this->entity)
                                    ->find($data['id']);

                    if (!empty($appointment->getGallery())) {
                        $files_dir = $appointment->getGallery();
                    } else {
                        $files_dir = md5(time());
                        mkdir("./public_html/uploads/appointment/" . $files_dir, 0777);
                    }

                    $path = "./public_html/uploads/appointment/" . $files_dir;

                    for ($i = 0; $i < $numFile; $i++) {
                        $name = $file['name'][$i];
                        $tmp  = $file['tmp_name'][$i];

                        $extension = @end(explode('.', $name));
                        $new_name = rand() . ".$extension";

                        move_uploaded_file($tmp, $path . '/' . $new_name);
                    }

                    $data['gallery'] = $files_dir;
                }
                
                if (
                    !$this->securityEvent->securityVerify($data['customer']) ||
                    !$this->securityEvent->securityVerify($data['description']) ||
                    !$this->securityEvent->securityVerify($data['price']) ||
                    !$this->securityEvent->securityVerify($data['notes']) ||
                    !$this->securityEvent->securityVerify($data['time']) 
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel([
            'form' => $form,
            'id' => $id
        ]);
    }

    public function deleteAction() {
        $conn = $this->em->getConnection();
        $query = "UPDATE customer SET deleted=:deleted WHERE id=:id";
        $stmt  = $conn->prepare($query);
        $stmt->bindValue(":deleted", 1);
        $stmt->bindValue(":id", $this->params()->fromRoute('id', 0));

        if ($stmt->execute())
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
    }

    public function detailsAction()
    {
        $id = $this->params()->fromRoute('id', 0);
        $appointment = $this->em->getRepository($this->entity)->find($id);

        $images = [];

        if (!empty($appointment->getGallery())) {
            $path = "./public_html/uploads/appointment/" . $appointment->getGallery() . '/';
            $op = opendir($path);
            $count = 0;
            while ($file = readdir($op)) {
                $ext = explode(".", $file);
                if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                    $count++;
                    $images[] = $count . "-" . $appointment->getGallery() . '/' . $file;
                }
            }
        }

        $model = new ViewModel();
        $model->setVariables([
            'appointment' => $appointment,
            'images' => $images
        ]);

        return $model;
    }

    public function galleryAction()
    {
        $id = $this->params()->fromQuery('id');

        $appointment = $this->em->getRepository($this->entity)
            ->find($id);

        $images = []; 

        if (!empty($appointment->getGallery())) {
            $path = "./public_html/uploads/appointment/" . $appointment->getGallery() . '/';
            $op = opendir($path);
            $count = 0;
            while ($file = readdir($op)) {
                $ext = explode(".", $file);
                if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                    $count++;
                    $images[] = $count . "-" . $appointment->getGallery() . '/' . $file;
                }
            }
        }

        $model = new ViewModel();
        $model->setTerminal(true);
        $model->setVariables([
            'images' => $images,
            'id' => $id
        ]);

        return $model;
    }

    public function deleteImageAction()
    {
        $params = $this->params()->fromQuery();

        $appointment = $this->em->getRepository($this->entity)
            ->find($params['id']);

        $images = []; 

        if (!empty($appointment->getGallery())) {
            $path = "./public_html/uploads/appointment/" . $appointment->getGallery() . '/';
            $op = opendir($path);
            $count = 0;
            while ($file = readdir($op)) {
                $ext = explode(".", $file);
                if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                    $count++;
                    if ($params['pos'] == $count) {
                        unlink($path . $file);
                        exit();
                    }
                }
            }
        }
    }

    public function freezeAction()
    {
        $id = $this->params()->fromRoute('id', 0);

        $query = "UPDATE appointment SET status=:status WHERE id=:id";
        $stmt  = $this->em->getConnection()->prepare($query);
        $stmt->bindValue(":status", "C");  
        $stmt->bindValue(":id", $id);  
        $stmt->execute();

        return $this->redirect()->toRoute($this->route, [
            'controller' => 'appointment',
            'action' => 'details',
            'id' => $id
        ]);
    }

    public function openAction()
    {
        $id = $this->params()->fromRoute('id', 0);

        $query = "UPDATE appointment SET status=:status WHERE id=:id";
        $stmt  = $this->em->getConnection()->prepare($query);
        $stmt->bindValue(":status", "O");  
        $stmt->bindValue(":id", $id);  
        $stmt->execute();

        return $this->redirect()->toRoute($this->route, [
            'controller' => 'appointment',
            'action' => 'details',
            'id' => $id
        ]);
    }

    public function doneAction()
    {
        $id = $this->params()->fromRoute('id', 0);

        $appointment = $this->em->getRepository($this->entity)
                            ->find($id);
        $query = "
            INSERT INTO sale (
                appointment_id, total, sale_date, user_id
            ) VALUES (
                :appointment_id, :total, :sale_date, :user_id
            )
        ";

        $stmt  = $this->em->getConnection()->prepare($query);
        $stmt->bindValue(":appointment_id", $appointment->getId());  
        $stmt->bindValue(":total", $appointment->getPrice());  
        $stmt->bindValue(":sale_date", date("Y-m-d H:i:s"));  
        $stmt->bindValue(":user_id", $this->getUserId());  
        
        if ($stmt->execute()) {
            $queryU = "
                UPDATE appointment SET status=:status WHERE id=:id
            ";

            $stmtU = $this->em->getConnection()->prepare($queryU);
            $stmtU->bindValue(":status", "D");
            $stmtU->bindValue(":id", $appointment->getId());

            if ($stmtU->execute()) {
                $cashier_amount = $this->cEvent->getCashierDailyReleases();
                $new_amount = $cashier_amount + $appointment->getPrice();
                $this->cEvent->updateDailyReleases($new_amount);
            } else {
                return false;
            }
        } else {
            return false;
        }
        
        return $this->redirect()->toRoute($this->route, [
            'controller' => 'sale'
        ]);
    }

    private function getUserId()
    {
        $sessionStorage = new SessionStorage("SellerControl");
        $this->authService = new AuthenticationService;
        $this->authService->setStorage($sessionStorage);

        if ($this->getAuthService()->hasIdentity()) {
            return $this->getAuthService()->getIdentity()->getId();
        } else {
            return false;
        }
    }
}