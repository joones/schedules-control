<?php

namespace SellerControl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;

class FinancialController extends AbstractActionController
{
	protected $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

    public function indexAction()
    {
        return new ViewModel();
    }
}
