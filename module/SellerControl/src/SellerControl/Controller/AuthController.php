<?php
namespace SellerControl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use SellerControl\Form\Login as LoginForm;
use SellerControl\Event\PasswordEvent;
use SellerControl\Event\CashierEvent;

class AuthController extends AbstractActionController {

    public function indexAction() {
        $form = new LoginForm;
        $error = false;
        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $sessionStorage = new SessionStorage("SellerControl");
                $auth = new AuthenticationService();

                $auth->setStorage($sessionStorage);

                $authAdapter = $this->getServiceLocator()->get("SellerControl\Auth\Adapter");

                $authAdapter->setUsername($data['email']);
                $authAdapter->setPassword($data['password']);

                $result = $auth->authenticate($authAdapter);
       
                if ($result->isValid()) {
                    $sessionStorage->write($auth->getIdentity()['user']);

                    $em = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager");
                    $cEvent = new CashierEvent($em);
                    $cEvent->process();
                    
                    return $this->redirect()->toRoute("app-router/default");
                } else {
                    $error = true;
                }
            }
        }
        $model = new ViewModel();
        $model->setTerminal(true);
        $model->setVariables([
            'form' => $form,
            'error' => $error
        ]);
        return $model;
    } 
    
    public function logoutAction() {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage("SellerControl"));
        $auth->clearIdentity();
        return $this->redirect()->toRoute('app-auth');
    }
}
