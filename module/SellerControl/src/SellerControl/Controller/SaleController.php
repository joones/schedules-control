<?php

namespace SellerControl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityManager;

class SaleController extends AbstractActionController
{
    protected $em;
	protected $entity;
    protected $saleItemsEntity;

	public function __construct(EntityManager $em, $entity, $saleItemsEntity)
	{
		$this->em     = $em;
        $this->entity = $entity;
		$this->saleItemsEntity = $saleItemsEntity;
	}

	public function indexAction() {
         
		$query  = "SELECT s FROM SellerControl\Entity\Sale s 
						ORDER BY s.id DESC";
		$stmt   = $this->em->createQuery($query);
		$list = $stmt->getResult();
		
        $page = $this->params()->fromRoute('page');
        
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
                ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }
	
    public function detailsAction()
    {
    	$id = $this->params()->fromRoute('id');
    	
    	$saleRepository = $this->em->getRepository($this->entity);
    	$sale = $saleRepository->find($id);
    	
        $appointment = $this->em->getRepository(
            'SellerControl\Entity\Appointment'
        )->find($sale->getAppointment()->getId());

        $images = []; 

        if (!empty($appointment->getGallery())) {
            $path = "./public_html/uploads/appointment/" . $appointment->getGallery() . '/';
            $op = opendir($path);
            $count = 0;
            while ($file = readdir($op)) {
                $ext = explode(".", $file);
                if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
                    $count++;
                    $images[] = $count . "-" . $appointment->getGallery() . '/' . $file;
                }
            }
        }

    	return new ViewModel([
    		'sale' => $sale,
    		'total' => $sale->getTotal(),
            'images' => $images
    	]);
    }
}







