<?php 

namespace SellerControl\Event;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;

class PasswordEvent implements EventManagerAwareInterface
{
	protected $events;
	private $conn;
	private $em;

	public function __construct($em)
	{
		$this->em   = $em;
		$this->conn = $this->em->getConnection();
	}

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_class($this)
        ));
        $this->events = $events;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    public function update($id, $password)
    {
    	$query = "UPDATE user SET password=:password WHERE id=:id";
    	$stmt  = $this->conn->prepare($query);
    	$stmt->bindValue(":password", $password);
    	$stmt->bindValue(":id", $id);
    	return $stmt->execute();
    }
}