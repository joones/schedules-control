<?php 

namespace SellerControl\Event;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

class CashierEvent implements EventManagerAwareInterface
{
	protected $events;
	private $conn;
	private $em;

	public function __construct($em)
	{
		$this->em   = $em;
		$this->conn = $this->em->getConnection();
	}

    public function getAuthService() {
        return $this->authService;
    }

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_class($this)
        ));
        $this->events = $events;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    public function process()
    {
        $cashier_repo = $this->em->getRepository(
            'SellerControl\Entity\Cashier'
        );

        $cashier = $cashier_repo->findByUser($this->getLoggedUser());

        if ($cashier) {
            $open_cashier = $cashier_repo->findOneBy([
                'user' => $this->getLoggedUser(),
                'closed' => '0'
            ]);                        

            if (!$open_cashier) {
                $close_cashier = $cashier_repo->findOneBy([
                    'user' => $this->getLoggedUser(),
                    'closed' => '1'
                ], ['id' => 'DESC']);

                if ($close_cashier->getDailyAmount() != 0) {
                    $amount = $close_cashier->getDailyAmount();
                } else {
                    $amount = $close_cashier->getAmount();
                }

                return $this->updateUserCashier($amount);                     
            }
        } else {
            return $this->createUserCashier();
        }
    }

    public function getResume()
    {
        try {
            $query = "
                SELECT amount, daily_releases, daily_expense FROM cashier 
                WHERE user_id=:user_id
                AND closed = :closed
                ORDER BY id DESC
            ";
             
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed", 0);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC); 
            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCashierAmount()
    {
        try {
            $query = "
                SELECT amount FROM cashier 
                WHERE user_id=:user_id
                AND closed = :closed
                ORDER BY id DESC
            ";
             
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed", 1);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC); 
             
            return $result['amount'];
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function getCashierDailyReleases()
    {
        try {
            $query = "
                SELECT daily_releases FROM cashier 
                WHERE user_id=:user_id
                AND closed = :closed
                ORDER BY id DESC
            ";
             
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed", 0);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC); 
             
            return $result['daily_releases'];
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCashierDailyExpenses()
    {
        try {
            $query = "
                SELECT daily_expense FROM cashier 
                WHERE user_id=:user_id
                AND closed = :closed
                ORDER BY id DESC
            ";
             
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed", 0);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
             
            return $result['daily_expense'];
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateDailyReleases($value)
    {
        try {
            $query = "
                UPDATE cashier SET daily_releases=:daily_releases, updated_at=:updated_at
                WHERE user_id=:user_id
                AND closed = :closed
                ORDER BY id DESC
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":daily_releases", $value);
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed", 0);
            return $stmt->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateDailyExpenses($value)
    {
        try {
            $query = "
                UPDATE cashier SET daily_expense=:daily_expense, updated_at=:updated_at
                WHERE user_id=:user_id
                AND closed = :closed
                ORDER BY id DESC
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":daily_expense", $value);
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed", 0);
            return $stmt->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateDailyAmount() 
    {
        try {
            $releases = $this->getCashierDailyReleases();
            $expenses = $this->getCashierDailyExpenses();
            $amount    = $this->getCashierAmount();

            $daily_amount = ($amount - $expenses) + $releases;

            $query = "
                UPDATE cashier SET daily_amount=:daily_amount, updated_at=:updated_at, closed=:closed
                WHERE user_id=:user_id
                AND closed = :closed_db 
                ORDER BY id DESC
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":daily_amount", $daily_amount);
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":closed", 1);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":closed_db", 0);

            if ($stmt->execute()) {
                if ($this->refreshCashier($daily_amount)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function refreshCashier($dailyAmount)
    {
        try {
            $amount =  $this->getCashierAmount();
            $new_amount = ($amount + $dailyAmount);
          
            $query = "
                INSERT INTO cashier 
                    (amount, daily_releases, daily_expense, daily_amount, 
                    user_id, created_at, updated_at, closed)
                VALUES (:amount, :daily_releases, :daily_expense, :daily_amount, 
                        :user_id, :created_at, :updated_at, :closed)
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":amount", $new_amount);
            $stmt->bindValue(":daily_releases", 0);
            $stmt->bindValue(":daily_expense", 0);
            $stmt->bindValue(":daily_amount", 0);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":created_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":closed", 0);

            return $stmt->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function createUserCashier()
    {
        try {
            $query = "
                INSERT INTO cashier 
                    (amount, daily_releases, daily_expense, daily_amount, 
                    user_id, created_at, updated_at, closed)
                VALUES (:amount, :daily_releases, :daily_expense, :daily_amount, 
                        :user_id, :created_at, :updated_at, :closed)
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":amount", 0);
            $stmt->bindValue(":daily_releases", 0);
            $stmt->bindValue(":daily_expense", 0);
            $stmt->bindValue(":daily_amount", 0);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":created_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":closed", 0);

            return $stmt->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function updateUserCashier($amount)
    {
        try {
            $query = "
                INSERT INTO cashier 
                    (amount, daily_releases, daily_expense, daily_amount, 
                    user_id, created_at, updated_at, closed)
                VALUES (:amount, :daily_releases, :daily_expense, :daily_amount, 
                        :user_id, :created_at, :updated_at, :closed)
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":amount", $amount);
            $stmt->bindValue(":daily_releases", 0);
            $stmt->bindValue(":daily_expense", 0);
            $stmt->bindValue(":daily_amount", 0);
            $stmt->bindValue(":user_id", $this->getLoggedUser());
            $stmt->bindValue(":created_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":updated_at", date("Y-m-d H:i:s"));
            $stmt->bindValue(":closed", 0);

            return $stmt->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function getLoggedUser()
    {
        $sessionStorage = new SessionStorage("SellerControl");
        $this->authService = new AuthenticationService;
        $this->authService->setStorage($sessionStorage);

        $identity = $this->getAuthService()->getIdentity()->getId();
        $userRepository = $this->em->getRepository('SellerControl\Entity\User');
        $user = $userRepository->find($identity);
        
        return $user->getId();
    }
}





