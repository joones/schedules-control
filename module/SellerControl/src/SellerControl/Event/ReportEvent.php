<?php 

namespace SellerControl\Event;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

class ReportEvent implements EventManagerAwareInterface
{
	protected $events;
	private $conn;
	private $em;

	public function __construct($em)
	{
		$this->em   = $em;
		$this->conn = $this->em->getConnection();
	}

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_class($this)
        ));
        $this->events = $events;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    public function executeReport($params)
    {
        $conditions = "";

        if ($params['module'] != "sale") {
            $conditions .= " AND deleted = '0' ";
            $date_field = "created_at";
        } else {
            $date_field = "sale_date";
        }

        if ($params['module'] == "account-payable") {
            $params['module'] = "account_payable_receivable";
            $conditions .= " AND type = 'P' ";
        }
        
        if ($params['module'] == "account-receivable") {
            $params['module'] = "account_payable_receivable";
            $conditions .= " AND type = 'R' ";
        }

        if ($params['module'] == "order-service") {
            $params['module'] = "order_service";
        }

        if (!empty($params['initial']) && empty($params['final'])) {
            $initial = $params['initial'] . " 00:00:00";
            $conditions .= " AND {$date_field} >= '" . $initial . "' ";
        }
        elseif (empty($params['initial']) && !empty($params['final'])) {
            $final   = $params['final'] . " 23:59:59";
            $conditions .= " AND {$date_field} <= '" . $final . "' ";
        }
        elseif (!empty($params['initial']) && !empty($params['final'])) {
            $initial = $params['initial'] . " 00:00:00";
            $final   = $params['final'] . " 23:59:59";
            $conditions .= " AND {$date_field} BETWEEN '" . $initial . "' AND '" . $final . "' ";
        }

        $query = "SELECT * FROM {$params['module']} WHERE 1 {$conditions}";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }   
}