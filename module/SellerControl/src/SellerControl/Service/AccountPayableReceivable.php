<?php 

namespace SellerControl\Service;

use Core\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;

class AccountPayableReceivable extends AbstractService
{
	public function __construct(\Doctrine\ORM\EntityManager $em, $entity)
	{
		parent::__construct($em);
		$this->entity = $entity;
	}

	public function insert(array $data) {
        $entity = new $this->entity($data);

        $entity->setType($data['type']);

        if ($data['customer'] != "0") {
            $customer = $this->em->getReference("SellerControl\Entity\Customer", $data['customer']);
            $entity->setCustomer($customer);    
        } else {
            $entity->setCustomer(null); 
        }

        if (isset($data['account_value'])) {
            $account_value = str_replace(',','.', str_replace('.','', $data['account_value']));
            $entity->setAccountValue($account_value);
        }

        if (isset($data['expiration'])) {
            $expiration = $data['expiration'];
            if ($expiration < date("Y-m-d")) {
                $entity->setStatus('E');
            } else {
                $entity->setStatus('O');
            }
        } else {
            $entity->setStatus('O');
        }

        $entity->setDeleted('0');

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
    
    public function update(array $data) {
        $entity = $this->em->getReference($this->entity, $data['id']);
        
        (new Hydrator\ClassMethods())->hydrate($data, $entity);

        if (isset($data['customer']) && $data['customer'] != "0") {
            $customer = $this->em->getReference("SellerControl\Entity\Customer", $data['customer']);
            $entity->setCustomer($customer);    
        } else {
            $entity->setCustomer(null); 
        }

        if (isset($data['account_value'])) {
            $account_value = str_replace(',','.', str_replace('.','', $data['account_value']));
            $entity->setAccountValue($account_value);
        }

        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }
}