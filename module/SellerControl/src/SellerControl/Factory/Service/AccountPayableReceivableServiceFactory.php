<?php

namespace SellerControl\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Service\AccountPayableReceivable;

class AccountPayableReceivableServiceFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {	
   		$em = $controllerManager->get('Doctrine\ORM\EntityManager');

        $service = new AccountPayableReceivable(
        	$em, 'SellerControl\Entity\AccountPayableReceivable'
        );
    	
        return $service;
    }
}