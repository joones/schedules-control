<?php

namespace SellerControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Form\Appointment;

class AppointmentFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$em = $controllerManager->get('Doctrine\ORM\EntityManager');

		$customerRepository = $em->getRepository('SellerControl\Entity\Customer');
   		$customers = $customerRepository->fetchCustomers();

        $form = new Appointment('appointment', $customers);
        return $form;
    }
}