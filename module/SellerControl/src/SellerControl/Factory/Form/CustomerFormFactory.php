<?php

namespace SellerControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Form\Customer;

class CustomerFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
        $form = new Customer('customer');
        return $form;
    }
}