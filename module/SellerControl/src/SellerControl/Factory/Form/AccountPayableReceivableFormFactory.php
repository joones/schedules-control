<?php

namespace SellerControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Form\AccountPayableReceivable;

class AccountPayableReceivableFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$em = $controllerManager->get('Doctrine\ORM\EntityManager');

		$customerRepository = $em->getRepository('SellerControl\Entity\Customer');
   		$customers = $customerRepository->fetchAccountCustomers();

        $form = new AccountPayableReceivable(
        	'account', $customers
        );
        
        return $form;
    }
}