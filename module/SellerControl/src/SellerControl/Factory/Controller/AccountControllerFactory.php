<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\AccountController;

class AccountControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');

        $form = $serviceManager->get('SellerControl\Form\AccountPayableReceivable');
    	$service = $serviceManager->get('SellerControl\Service\AccountPayableReceivable');

        $controller = new AccountController(
            $em,
        	'SellerControl\Entity\AccountPayableReceivable', 
        	$form, $service, 'account', 'app-router/default'
        );
    	
        return $controller;
    }
}