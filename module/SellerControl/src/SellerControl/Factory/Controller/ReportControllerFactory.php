<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\ReportController;

class ReportControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();
        $em = $serviceManager->get('Doctrine\ORM\EntityManager');

        $form = $serviceManager->get('SellerControl\Form\Report');

        $controller = new ReportController(
    		$em, $form
    	);

        return $controller;
    }
}