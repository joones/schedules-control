<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\UserController;

class UserControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
    	$form = $serviceManager->get('SellerControl\Form\User');
    	$service = $serviceManager->get('SellerControl\Service\User');

        $controller = new UserController(
    		$em, 'SellerControl\Entity\User', $form, $service, 'user', 'app-router/default'   
    	);
    	
        return $controller;
    }
}