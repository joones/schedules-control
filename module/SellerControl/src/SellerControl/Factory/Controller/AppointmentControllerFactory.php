<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\AppointmentController;

class AppointmentControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
    	$form = $serviceManager->get('SellerControl\Form\Appointment');
    	$service = $serviceManager->get('SellerControl\Service\Appointment');

        $controller = new AppointmentController(
    		$em, 'SellerControl\Entity\Appointment', $form, $service, 'appointment', 'app-router/default'
    	);
        
        return $controller;
    }
}