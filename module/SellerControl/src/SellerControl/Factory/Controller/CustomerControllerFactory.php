<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\CustomerController;

class CustomerControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
    	$form = $serviceManager->get('SellerControl\Form\Customer');
    	$service = $serviceManager->get('SellerControl\Service\Customer');

        $controller = new CustomerController(
    		$em, 'SellerControl\Entity\Customer', $form, $service, 'customer', 'app-router/default'
    	);
        
        return $controller;
    }
}