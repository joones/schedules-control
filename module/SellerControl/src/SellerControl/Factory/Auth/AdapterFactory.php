<?php

namespace SellerControl\Factory\Auth;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Auth\Adapter;

class AdapterFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {	
   		$em = $controllerManager->get('Doctrine\ORM\EntityManager');

        $service = new Adapter($em);
    	
        return $service;
    }
}