<?php

namespace SellerControl\View\Helper;

use Zend\View\Helper\AbstractHelper;

class AppointmentCustomerName extends AbstractHelper {

    public function __invoke($id) {
    	$sm = $this->getView()->getHelperPluginManager()->getServiceLocator();
    	$em = $sm->get('Doctrine\ORM\EntityManager');
    	
    	$customerRepository = $em->getRepository('SellerControl\Entity\Appointment');
    	$customer = $customerRepository->find($id);
    	
    	return $customer->getCustomer()->getName();
    }

}
