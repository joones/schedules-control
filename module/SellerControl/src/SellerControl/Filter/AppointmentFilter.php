<?php  

namespace SellerControl\Filter;

use Zend\InputFilter\InputFilter;

class AppointmentFilter extends InputFilter
{
	public function __construct()
	{
		$this->add([
			'name' => 'description',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo DESCRIÇÃO.',
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'time',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo DATA/HORA.',
						]
					]
				]
			]
		]);
	}
}