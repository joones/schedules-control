<?php  

namespace SellerControl\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;

class CustomerFilter extends InputFilter
{
	public function __construct()
	{
		$this->add([
			'name' => 'name',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo NOME.',
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'email',
			'validators' => [
				[
			        'name' => 'EmailAddress',
			        'options' => [
                        'required'  => true,
			            'messages' => [
			                'emailAddressInvalidFormat' => "Digite um EMAIL VÁLIDO.",

			            ]
			        ],
			    ],
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo EMAIL.',
						]
					]
				],
				
			]
		]);

		$this->add([
			'name' => 'phone',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo TELEFONE.',
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'cellphone',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo CELULAR.',
						]
					]
				]
			]
		]);
	}
}