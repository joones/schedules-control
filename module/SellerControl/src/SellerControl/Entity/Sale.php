<?php 

namespace SellerControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="sale")
 * @ORM\Entity(repositoryClass="SellerControl\Repository\SaleRepository")
 */
class Sale
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
     * @ORM\OneToOne(targetEntity="SellerControl\Entity\Appointment")
     * @ORM\JoinColumn(name="appointment_id", referencedColumnName="id")
     */
    private $appointment;

    /**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $total;

	/**
	 * @ORM\Column(type="datetime", name="sale_date")
	 * @var string
	 */
	private $saleDate;

	/**
     * @ORM\OneToOne(targetEntity="SellerControl\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
		$this->saleDate = new \DateTime("now");
	}

	function getId()
	{
		return $this->id;
	}

	function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	function getAppointment()
	{
		return $this->appointment;
	}

	function setAppointment($appointment)
	{
		$this->appointment = $appointment;
		return $this;
	}

	function getTotal()
	{
		return $this->total;
	}

	function setTotal($total)
	{
		$this->total = $total;
		return $this;
	}

	function getSaleDate() {
        return $this->saleDate;
    }
    
    function setSaleDate(\DateTime $saleDate) {
        $this->saleDate = $saleDate;
        return $this;
    }
  
    function getUser()
	{
		return $this->user;
	}

	function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

    public function toArray() {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}	