<?php 

namespace SellerControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="account_payable_receivable")
 * @ORM\Entity(repositoryClass="SellerControl\Repository\AccountPayableReceivableRepository")
 */
class AccountPayableReceivable
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $type;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $document;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $description;

	/**
	 * @ORM\Column(type="text", name="account_value")
	 * @var string
	 */
	protected $accountValue;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $emission;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $expiration;

	/**
	 * @ORM\Column(type="text", name="pay_day")
	 * @var string
	 */
	protected $pay_day;

	/**
	 * @ORM\OneToOne(targetEntity="SellerControl\Entity\Customer")
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
	 */
	protected $customer;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $status;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $notes;

	/**
	 * @ORM\Column(type="datetime", name="created_at")
	 * @var string
	 */
	protected $created;

	/**
	 * @ORM\Column(type="datetime", name="updated_at")
	 * @var string
	 */
	protected $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="string")
     */
    private $deleted;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
		$this->created = new \DateTime("now");
		$this->updated = new \DateTime("now");
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return AccountPayableReceivable
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return AccountPayableReceivable
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDocument()
	{
		return $this->document;
	}

	/**
	 * @param string $document
	 * @return AccountPayableReceivable
	 */
	public function setDocument($document)
	{
		$this->document = $document;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return AccountPayableReceivable
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAccountValue()
	{
		return $this->accountValue;
	}

	/**
	 * @param string $accountValue
	 * @return AccountPayableReceivable
	 */
	public function setAccountValue($accountValue)
	{
		$this->accountValue = $accountValue;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEmission()
	{
		return $this->emission;
	}

	/**
	 * @param string $emission
	 * @return AccountPayableReceivable
	 */
	public function setEmission($emission)
	{
		$this->emission = $emission;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getExpiration()
	{
		return $this->expiration;
	}

	/**
	 * @param string $expiration
	 * @return AccountPayableReceivable
	 */
	public function setExpiration($expiration)
	{
		$this->expiration = $expiration;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPayDay()
	{
		return $this->pay_day;
	}

	/**
	 * @param string $pay_day
	 * @return AccountPayableReceivable
	 */
	public function setPayDay($pay_day)
	{
		$this->pay_day = $pay_day;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * @param mixed $customer
	 * @return AccountPayableReceivable
	 */
	public function setCustomer($customer)
	{
		$this->customer = $customer;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return AccountPayableReceivable
	 */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNotes()
	{
		return $this->notes;
	}

	/**
	 * @param string $notes
	 * @return AccountPayableReceivable
	 */
	public function setNotes($notes)
	{
		$this->notes = $notes;
		return $this;
	}

	public function getCreated() {
        return $this->created;
    }
    
    public function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
  
    public function getUpdated() {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

	/**
	 * @return string
	 */
	public function getDeleted()
	{
		return $this->deleted;
	}

	/**
	 * @param string $deleted
	 * @return AccountPayableReceivable
	 */
	public function setDeleted($deleted)
	{
		$this->deleted = $deleted;
		return $this;
	}

	public function toArray() {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}	