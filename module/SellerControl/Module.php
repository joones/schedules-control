<?php

namespace SellerControl;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\ModuleManager\ModuleManager;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function init(ModuleManager $moduleManager) {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        
        $sharedEvents->attach("Zend\Mvc\Controller\AbstractActionController", 
                MvcEvent::EVENT_DISPATCH,
                array($this,'mvcPreDispatch'),100);
    }
    
    public function mvcPreDispatch($e) {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage("SellerControl"));
        
        $controller = $e->getTarget();
        $matchedRoute = $controller->getEvent()->getRouteMatch()->getMatchedRouteName();

        if ($matchedRoute == "app-router")
            return $controller->redirect()->toRoute("app-home");

        if ($matchedRoute != "app-password-recover" ) {
            if(!$auth->hasIdentity() and ($matchedRoute == "app-router" OR $matchedRoute == "app-router/default" OR $matchedRoute == "app-router/paginator")) {
                return $controller->redirect()->toRoute("app-auth");
            }
        }
    }
    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'SellerControl\Auth\Adapter' => 'SellerControl\Factory\Auth\AdapterFactory',
                'SellerControl\Form\User' => 'SellerControl\Factory\Form\UserFormFactory',
                'SellerControl\Form\Customer' => 'SellerControl\Factory\Form\CustomerFormFactory',
                'SellerControl\Form\Appointment' => 'SellerControl\Factory\Form\AppointmentFormFactory',
                'SellerControl\Form\AccountPayableReceivable' => 'SellerControl\Factory\Form\AccountPayableReceivableFormFactory',
                'SellerControl\Form\Report' => 'SellerControl\Factory\Form\ReportFormFactory',
                'SellerControl\Service\User' => 'SellerControl\Factory\Service\UserServiceFactory',
                'SellerControl\Service\Customer' => 'SellerControl\Factory\Service\CustomerServiceFactory',
                'SellerControl\Service\Appointment' => 'SellerControl\Factory\Service\AppointmentServiceFactory',
                'SellerControl\Service\AccountPayableReceivable' => 'SellerControl\Factory\Service\AccountPayableReceivableServiceFactory',
            ]
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                'SellerControl\Controller\User' => 'SellerControl\Factory\Controller\UserControllerFactory',
                'SellerControl\Controller\Customer' => 'SellerControl\Factory\Controller\CustomerControllerFactory',
                'SellerControl\Controller\Appointment' => 'SellerControl\Factory\Controller\AppointmentControllerFactory',
                'SellerControl\Controller\Sale' => 'SellerControl\Factory\Controller\SaleControllerFactory',
                'SellerControl\Controller\Cashier' => 'SellerControl\Factory\Controller\CashierControllerFactory',
                'SellerControl\Controller\Account' => 'SellerControl\Factory\Controller\AccountControllerFactory',
                'SellerControl\Controller\Report' => 'SellerControl\Factory\Controller\ReportControllerFactory',
                'SellerControl\Controller\SystemConfig' => 'SellerControl\Factory\Controller\SystemConfigControllerFactory',
                'SellerControl\Controller\Financial' => 'SellerControl\Factory\Controller\FinancialControllerFactory',
            ],
            'invokables' => [
            'SellerControl\Controller\Index' => 'SellerControl\Controller\IndexController',
            'SellerControl\Controller\Auth' => 'SellerControl\Controller\AuthController',
            ]
        ];
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig() 
    {
        return array(
            'invokables' => array(
                'UserIdentity'  => 'SellerControl\View\Helper\UserIdentity',
                'StatusName'    => 'SellerControl\View\Helper\StatusName',
                'CompleteUserIdentity' => 'SellerControl\View\Helper\CompleteUserIdentity',
                'GetControllerRoute' => 'SellerControl\View\Helper\GetControllerRoute',
                'CustomerName'  => 'SellerControl\View\Helper\CustomerName',
                'ShowAppointmentImage'  => 'SellerControl\View\Helper\ShowAppointmentImage',
                'AppointmentCustomerName'  => 'SellerControl\View\Helper\AppointmentCustomerName',
            ),
        );
    }
}